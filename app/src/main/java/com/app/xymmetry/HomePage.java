package com.app.xymmetry;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class HomePage extends AppCompatActivity {

    RecyclerView postRecyclerView;
    RecyclerAdapter postRecyclerViewAdapter;
    List<Post> postList;
    public static final String POST_TEXT = "A paragraph is a self-contained unit of a discourse in writing dealing with a particular point or idea. A paragraph consists of one or more sentences. Though not required by the syntax of any language, paragraphs are usually an expected part of formal writing, used to organize longer prose.";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_page);

        init();
        setData();
        setAdapters();
    }

    public void init() {
        postRecyclerView = findViewById(R.id.post_recycler_view);
        postList = new ArrayList<>();
    }


    public void setData() {

        postList.add(new Post(R.drawable.profile_pic, R.drawable.one, POST_TEXT));
        postList.add(new Post(R.drawable.profile_pic, R.drawable.two, POST_TEXT));
        postList.add(new Post(R.drawable.profile_pic, R.drawable.three, POST_TEXT));
        postList.add(new Post(R.drawable.profile_pic, R.drawable.four, POST_TEXT));
        postList.add(new Post(R.drawable.profile_pic, R.drawable.five, POST_TEXT));
        postList.add(new Post(R.drawable.profile_pic, R.drawable.six, POST_TEXT));
        postList.add(new Post(R.drawable.profile_pic, R.drawable.seven, POST_TEXT));
        postList.add(new Post(R.drawable.profile_pic, R.drawable.eight, POST_TEXT));
        postList.add(new Post(R.drawable.profile_pic, R.drawable.nine, POST_TEXT));
        postList.add(new Post(R.drawable.profile_pic, R.drawable.ten, POST_TEXT));
    }
//

    public void setAdapters() {

        postRecyclerViewAdapter = new RecyclerAdapter(postList);
        postRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        postRecyclerView.setAdapter(postRecyclerViewAdapter);
    }
}
