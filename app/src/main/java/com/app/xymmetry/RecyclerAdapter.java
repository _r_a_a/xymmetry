package com.app.xymmetry;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {

    List<Post> postList;

    public RecyclerAdapter(List<Post> postList) {
        this.postList = postList;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        CircleImageView profilePic;
        ImageView optionButton, postPic;
        TextView postText;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            profilePic = itemView.findViewById(R.id.profile_pic);
            optionButton = itemView.findViewById(R.id.option_btn);
            postPic = itemView.findViewById(R.id.post_pic);
            postText = itemView.findViewById(R.id.text_post);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.recycler_view_row, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        myViewHolder.profilePic.setImageResource(postList.get(i).getProfilePic());
        myViewHolder.postPic.setImageResource(postList.get(i).getPostImage());
        myViewHolder.postText.setText(postList.get(i).getPostText());

    }

    @Override
    public int getItemCount() {
        return postList.size();
    }
}
