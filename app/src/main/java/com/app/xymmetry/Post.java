package com.app.xymmetry;

public class Post {

    private int profilePic;
    private int postImage;
    private String postText;

    public Post(int profilePic, int postImage, String postText) {
        this.profilePic = profilePic;
        this.postImage = postImage;
        this.postText = postText;
    }

    public int getProfilePic() {
        return profilePic;
    }

    public int getPostImage() {
        return postImage;
    }

    public String getPostText() {
        return postText;
    }
}
