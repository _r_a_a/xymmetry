package com.app.xymmetry;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button loginSuccessfulBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    public void init() {
        loginSuccessfulBtn = findViewById(R.id.login_succesful_btn);
        loginSuccessfulBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.login_succesful_btn :
                Intent intent = new Intent(this, HomePage.class);
                startActivity(intent);
                break;
            default: //do nothing;
        }
    }
}
